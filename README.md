# UserAuthentication

The project provides a basic multi threaded user authentication experience.
I've used Java, along with Springboot for the backend, and Java Swing for the Frontend.
For the database management, I'm using Spring's `JpaRepository`.

## Cloning the project

`git clone https://gitlab.com/srijeet0406/userauthentication.git` 

## Running the project

Once you have cloned the repo, go into the directory and run

`mvn clean install`

`mvn clean verify`

If everything goes fine, you should have a jar file that you can run like

`java -jar target/UserAuthentication-1.0.0.jar`

By default, the app runs on port `8080`

## Interacting with the project

The project uses REST endpoints to interact with the user for the various operations. The following are the different endpoints that you can interact with:

### GET all users

`GET http://localhost:8080/users`

To start with, this should just give you an empty list.

### GET a specific user

`GET http://localhost:8080/users/<user name>`

If the user exists, you will get back a `200` response code, else you should get a `404`.

### DELETE a specific user

`DELETE http://localhost:8080/users/<user name>`

If the user exists, you will get back a `200` response code, else you should get a `404`.

### PUT  new user

`PUT http://localhost:8080/users/<user name>`

This method is handled from within the app for the most part.

### Hit the /authorization endpoint

`GET http://localhost:8080/authorization`

This starts a thread, which launches the UI for the user to enter the username and password. 
This handles both existing, as well as new users.
For existing users, if you enter the wrong password more than 4 times, you will be locked out of your account.
For new users, you have to follow the password guidelines for a new password. They are as follows:

`
1.) Password must be atleast 8 characters long
`

`
2.) Password must have atleast one numeric value
`

`
3.) Password must have atleast one special character
`

After registering a new user, if you hit the endpoint to get all users, it should give you an updated list of users.

## Future work

1.) Write tests for the app

2.) Implement the account locking mechanism

3.) Use some kind of javascript framework for UI, instead of Java Swing

4.) Use some kind of logging framework instead of `System.out.Println`

## Contact

`Srijeet Chatterjee`

`srijeet0406@gmail.com`

`Gitlab link: https://gitlab.com/srijeet0406`