package auth;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Optional;

public class LaunchUI implements Runnable{

    static {
        System.setProperty("java.awt.headless", "false");
    }

    private AuthController authController;
    private JPanel jPanel;
    private JTextField userName;
    private JLabel labelUser;
    private JPasswordField jPassword;
    private JLabel labelPass;
    private JRadioButton viewPswd;
    private JButton ok;
    private JButton cancel;
    private int lockout;

    public LaunchUI(AuthController authController) {
        this.authController = authController;
        this.lockout = 0;
    }

    public void initUI() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException, InterruptedException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        //The JPanel and it's contents
        jPanel = new JPanel();
        userName = new JTextField(8);
        userName.setToolTipText("Enter Your Username");
        labelUser = new JLabel("Username:");
        jPassword = new JPasswordField(8);
        jPassword.setToolTipText("Enter Your Password");
        labelPass = new JLabel("Password:");
        viewPswd = new JRadioButton();
        viewPswd.setText("View your Password");
        viewPswd.setToolTipText("To View Your Password");
        jPassword.setEchoChar('*');

        ok = new JButton("OK");
        cancel = new JButton("Cancel");

        //Adds the different items to the JPanel.
        jPanel.add(labelUser);
        jPanel.add(userName);
        jPanel.add(labelPass);
        jPanel.add(jPassword);
        jPanel.add(viewPswd);
        launch();
    }
    public void launch() throws InterruptedException, ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        ActionListener viewPswdActionListener = new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                if(viewPswd.isSelected()){
                    //Makes the Text in the JPasswordField visible.
                    jPassword.setEchoChar((char)0);
                }else{
                    //Sets it back to the default "**"
                    jPassword.setEchoChar('*');
                }
            }
        };

        viewPswd.addActionListener(viewPswdActionListener);
        //Specifies what buttons are on the JOptionPane
        String[] buttons = {"OK", "Cancel"};
        //Code for the JOptionPane
        int result = JOptionPane.showOptionDialog(null, jPanel, "Authentication", JOptionPane.WARNING_MESSAGE, 1, null, buttons, buttons);
        switch (result){
            case 0:
                if (userName.getText().equals("") || jPassword.getPassword().toString().equals("")) {
                    //Adds 1 to the int lockout 1 and if it reaches 4 it closes the app
                    lockout++;
                    if (lockout == 4) {
                        JOptionPane.showMessageDialog(null, "Please try again later", "Try Again Later", 0);
                        System.exit(0);
                    }
                    JOptionPane.showMessageDialog(null, "Please enter all of your credentials before continuing", "Try Again", 2);
                    initUI();
                } else {
                    // Existing user
                    if (this.authController.getRepository().existsById(userName.getText())){
                        Optional<User> user = this.authController.getRepository().findById(userName.getText());
                        if (user.get().getPswd().equals(String.valueOf(jPassword.getPassword()))) {
                            JOptionPane.showMessageDialog(null, "Correct", "Correct", 1);
                            Thread.currentThread().join();
                        } else {
                            JOptionPane.showMessageDialog(null, "Incorrect", "Incorrect", 0);
                            lockout++;
                            System.out.println("User " + user.get().getUserName() + " entered an incorrect pswd. Attempt number " + lockout);
                            if (lockout == 4) {
                                JOptionPane.showMessageDialog(null, "You've been locked out, pls try again later.", "Try Again Later", 0);
                                //ToDo: Add logic to include a "time since last locked out attempt" here
                                Thread.currentThread().join();
                            } else {
                                launch();
                            }
                        }
                    } else {
                        //New user
                        if (!PasswordProcessor.isValid(String.valueOf(jPassword.getPassword()))) {
                            JOptionPane.showMessageDialog(null, "Your password doesn't meet the minimum requirements.", "Invalid Password", 1);
                            initUI();
                        }
                        else {
                            this.authController.newUserWithPswd(new User(userName.getText(), String.valueOf(jPassword.getPassword())));
                            System.out.println("New user "+ userName.getText() + " registered.");
                            JOptionPane.showMessageDialog(null, "New User Registered!", "Register", 1);
                            Thread.currentThread().join();
                        }
                    }
                }
                break;
            case 1:
                Thread.currentThread().join();
                break;
            default:

        }
        JOptionPane.showOptionDialog(null, jPanel, "Authentication", JOptionPane.WARNING_MESSAGE, 1, null, null, null);
    }

    @Override
    public void run() {
        System.out.println("Invoking the UI here");
        try {
            initUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
