package auth;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class User {

    @Id
    private String userName;
    private String pswd;

    public User(){
        this.userName = "userName";
        this.pswd = "pswd";
    }
    public User(String userName, String pswd) {
        this.userName = userName;
        this.pswd = pswd;
    }

    public String getUserName() {
        return userName;
    }

    public String getPswd() {
        return pswd;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }

}

