package auth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordProcessor {
    //Password strength/validity checker
    public static boolean isValid(String pswd) {
        if (pswd == null || pswd.equals("")) {
            return false;
        }
        if (pswd.length() < 8) {
            return false;
        }
        /*Check to see if the pswd contains atleast one special character*/
        Pattern pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pswd);
        if (!matcher.find()) {
            return false;
        }
        /*Check to see if the pswd contains atleast one number*/
        pattern = Pattern.compile("([0-9])", Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(pswd);
        if (!matcher.find()) {
            return false;
        }
        return true;
    }
}
