package auth;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class AuthController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private final UserRepository repository;

    public UserRepository getRepository() {
        return repository;
    }

    AuthController(UserRepository repository) {
        this.repository = repository;
    }

    //Add a new user
    @RequestMapping("/authorization")
    public void newUser() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        LaunchUI ui = new LaunchUI(this);
        Thread t = new Thread(ui);
        t.start();
    }

    //Return all users
    @GetMapping("/users")
    public List<String> getUsers() {
        List<User> list = repository.findAll();
        List<String> listUsernames = new ArrayList<String>();
        for (User user : list) {
            listUsernames.add(user.getUserName());
        }
        return listUsernames;
    }

    //Put a new user
    @RequestMapping(method = RequestMethod.PUT, value = "/users/{userName}", produces = "application/json")
    Optional<User> newUserWithPswd(@RequestBody User newUser) {
        repository.save(newUser);
        return repository.findById(newUser.getUserName());
    }

    //Get a new user
    @RequestMapping(method = RequestMethod.GET, value = "/users/{userName}", produces = "application/json")
    public ResponseEntity<Void> getUser(@PathVariable String userName) {
        if (repository.existsById(userName)) {
            System.out.println("User " + userName + " found.");
            Optional<User> user = repository.findById(userName);
            if (user!= null) {
                return ResponseEntity.ok().build();
            }
        }
        System.out.println("User " + userName + " does not exist.");
        return ResponseEntity.notFound().build();
    }

    //Delete an existing user
    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{userName}", produces = "application/json")
    public ResponseEntity<Void> deleteUser(@PathVariable String userName) {
        if (repository.existsById(userName)) {
            repository.deleteById(userName);
            System.out.println("User " + userName + " deleted.");
            return ResponseEntity.ok().build();
        } else {
            System.out.println("User " + userName + " does not exist.");
            return ResponseEntity.notFound().build();
        }
    }
}
